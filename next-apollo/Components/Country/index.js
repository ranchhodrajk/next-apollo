import React from "react";
import {COUNTRYBYID} from "../../Query/Country/Country";
import { useQuery } from "@apollo/client";
import countries from 'get-countries-info';

const Country = () => {
    const { loading, error, data } = useQuery(COUNTRYBYID);

    let query = {
        name: 'Canada', //country name
    }

    console.log('data',data);

    console.log('countries({});',countries({},'name'));
    console.log('capital({});',countries(query,'capital'));
    console.log('India All City',countries(query, 'provinces'));


  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error</p>;
  return <div>

      {
          data?.countries?.map((item)=>(
              <div style={{display:'flex'}} key={item.code}>
                  <div>{item.name}</div>
                  <div>{item.emoji}</div>
              </div>
          ))
      }

  </div>;
};

export default Country;
