import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import TempratureCard from "../TempratureCard";
import { useQuery } from "@apollo/client";
import {COUNTRYBYID} from '../../Query/Country/Country'

const CountrySlider = () => {
  
  //  292968
  let getCityByIdId = [  "1273294", "6094817","1642911","292968","1699805", "524901","5128581", "1174872", "1850144","1796236", "1701668",   "2988507"]

  const { loading, error, data } = useQuery(COUNTRYBYID, {
    variables: {getCityByIdId},
  });

  const settings = {
    // dots: true,
    lazyLoad: true,
    infinite: true,
    speed: 500,
    slidesToShow: 6,
    slidesToScroll: 1,
    initialSlide: 2,
  };

  return (
    <div>
      <Slider {...settings}>
        {data?.getCityById?.map((item)=>{
            return(
                <div key={item?.id}>
            <TempratureCard
              cityName={item?.name}
              countryName={item?.country}
              temp={item?.weather?.temperature?.actual}
              icon={item?.weather?.summary?.icon}
              des={item?.weather?.summary?.description}
            />
          </div>
            )
        })}
      </Slider>
    </div>
  );
};

export default CountrySlider;

// import React, { Component } from "react";
// import Slider from "react-slick";
// import TempratureCard from "../TempratureCard";

// export default class LazyLoad extends Component {
//   render() {

//     console.log('This',this.props.data);

//     const settings = {
//       dots: true,
//       lazyLoad: true,
//       infinite: true,
//       speed: 500,
//       slidesToShow: 6,
//       slidesToScroll: 1,
//       initialSlide: 2
//     };
//     return (
//       <div>
//         <Slider {...settings}>
//           <div>
//             <TempratureCard cityName='Malbourn' countryName='AU' temp='20' icon='10d@2x' des='CLEAR SKY'/>
//           </div>

//         </Slider>
//       </div>
//     );
//   }
// }
