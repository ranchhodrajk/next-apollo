import React from 'react'
import Footer from '../Footer'
import NavBar from '../NavBar'
import s from '../../styles/Layout.module.scss'
const Layout = ({ children }) => {
    return (
        <div>
            <div className={s.mainLayout}>
                <NavBar/>
                    <div className={s.chlidContainer} key={Math.random()}>{children}</div>
                <Footer/>
            </div>
        </div>
    )
}

export default Layout
