import React,{useEffect,useState} from 'react'
import s from '../../styles/LiveClock.module.scss'

const LiveClock = () => {

    const [time, settime] = useState('')

    function timeFormate(val){
        if(val<10){
            return '0'
        }
        else{
            return ''
        }
    }

    useEffect(() => {
       const timerId = setInterval(()=>tike(),1000)

       return function cleanup(){
           clearInterval(timerId);
       }
    })

    function tike(){
        const d =new Date();
        const h= d.getHours();
        const m=d.getMinutes();
        const s=d.getSeconds();

        settime(timeFormate(h) + h + ':' + timeFormate(m) + m + ':' + timeFormate(s) + s  );
    }

    return (
        <div>
            <div className={s.timer}>
                <div className={s.timerData}>{time}</div>
                <div className={s.timerHead}>Live in the moment</div>
              </div>
        </div>
    )
}

export default LiveClock
