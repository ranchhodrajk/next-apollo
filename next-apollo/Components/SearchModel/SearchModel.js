import React, { useState, useEffect } from "react";
import { AutoComplete } from "antd";
import countries from "get-countries-info";
import Link from 'next/link'

const { Option } = AutoComplete;

const SearchModel = () => {
  const query = {
    name: "India", //country name
  };

  const [search, setsearch] = useState("");
  const [country, setcountry] = useState(countries(query, 'provinces'));




  const onClickState = (item) => {
    console.log('Click',item);
  }

  const handleSearch = (value) => {
    setsearch(value);


  };

  return (
    <AutoComplete onSearch={handleSearch} placeholder="Search your city">
      {country[0]
        .filter((val) => {
          if (search === "") {
            return "";
          } else if (val.toLowerCase().includes(search?.toLowerCase())) {
            return val;
          }
        })
        .map((item) => {
          return (
            <Option key={item} value={item} style={{  maxHeight: 100 }}>
              <Link href={`/${item}`}><a>{item}</a></Link>
              {/* <div onClick={()=>onClickState(item)}>{item}</div> */}
            </Option>
          );
        })}

      {/* {result.map((email) => (
        <Option key={email} value={email} style={{ width: 400 }}>
          {email}
        </Option>
      ))} */}
    </AutoComplete>
  );
};

export default SearchModel;

// import React, { useState, useEffect } from "react";
// import { Modal, Button } from "antd";
// import s from "../../styles/SearchModel.module.scss";
// import { SearchOutlined } from "@ant-design/icons";
// import countries from "get-countries-info";

// const SearchModel = () => {
//   const [isModalVisible, setIsModalVisible] = useState(false);
//   const [search, setsearch] = useState("");
//   const [country, setcountry] = useState(countries({}, "name"));
//   let query = {
//     name: "Canada", //country name
//   };

//   console.log("countries({});", countries({}, "name"));
//   console.log("capital({});", countries(query, "capital"));
//   console.log("India All City", countries(query, "provinces"));

//   const showModal = () => {
//     setIsModalVisible(true);
//   };

//   const handleCancel = () => {
//     setIsModalVisible(false);
//   };

// const onChangeSearch = (e) => {
//   setsearch(e.target.value);
// };

//   useEffect(() => {
//     console.log("search", search);
//   }, [search]);

//   return (
//     <div className={s.mainModal}>
//       <button type="primary" className={s.searchButton} onClick={showModal}>
//         Click to search
//       </button>
//       <Modal
//       centered
//         visible={isModalVisible}
//         onCancel={handleCancel}
//         className={s.searchModel}
//         footer={null}
//       >
//         <div className="SearchBoxWrap">
//           <input
//             autocomplete="off"
//             type="text"
//             name="searchBox"
//             className="SearchInput"
//             placeholder="Search your country"
//             onChange={onChangeSearch}
//           />
//           <SearchOutlined className="searchIcon" />
//         </div>
//         {country
//           .filter((val) => {
//             if (search === "") {
//               return "";
//             } else if (val.toLowerCase().includes(search.toLowerCase())) {
//               return val;
//             }
//           })
//           .map((item) => {
//             return (
//               <div className="searchResultBox" key={Math.random()}>
//                 <div className="searchReasultCountry">{item}</div>
//                 <div className="searchReasultCapital">Delhi</div>
//               </div>
//             );
//           })}

//       </Modal>
//     </div>
//   );
// };

// export default SearchModel;
