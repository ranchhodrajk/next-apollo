import React, { useState, useEffect } from "react";
import s from "../../styles/TempratureCard.module.scss";
import { kelvinToC } from "../../utilities/Converter";
const TempratureCard = ({ cityName, countryName, temp, icon, des }) => {
  const [path, setpath] = useState("");
  const [temprature, settemprature] = useState();

  useEffect(() => {
    settemprature(kelvinToC(temp));

    // if(temprature<10){
    //   setpath('/Assets/snow.gif');
    // }
  }, []);

  return (
    <div
      className={s.mainCardContainer}
      style={{
        backgroundImage: temprature <= 10 ? `url(/Assets/snow.gif)` 
        :  temprature >10 && temprature <=20? "url(/Assets/normalCold.gif)"
        :  temprature >20 && temprature <=30? "url(/Assets/normal.gif)"
        :  temprature > 30 ? "url(/Assets/hotSummer.gif)" : "",
      }}
    >
      <div className={s.Name}>
        <div className={s.cityName}>{cityName}</div>
        <div className={s.countryName}>{countryName}</div>
      </div>
      <div className={s.dgeree}>
        <div className={s.dgereeNum}>{temprature}</div>
        <div className={s.dgereeo}>o</div>
        <div className={s.dgereec}>c</div>
      </div>
      <div className={s.tempImg}>
        <img src={`http://openweathermap.org/img/wn/${icon}@2x.png`} alt="iconWeather" />
      </div>
      <div className={s.summary}>{
         des?.length >= 20
         ? des?.slice(0, 20) + "..."
         : des
      }</div>
    </div>
  );
};

export default TempratureCard;
