import { gql } from "@apollo/client";
export const COUNTRYBYID = gql`
query GetCityById($getCityByIdId: [String!]) {
  getCityById(id: $getCityByIdId) {
    id
    name
    country
    weather {
      summary {
        title
        description
        icon
      }
      temperature {
        actual
        feelsLike
        min
        max
      }
    }
  }
}
`;

export const COUNTRY = gql`
query GetCity($name: String!) {
  getCityByName(name: $name) {
    id
    name
    country
    coord {
      lon
      lat
    }
    weather {
      summary {
        title
        description
        icon
      }
      temperature {
        actual
        feelsLike
      }
      wind {
        speed
        deg
      }
      clouds {
        visibility
        humidity
        all
      }
      timestamp
    }
  }
}

`;

// export const COUNTRY = gql`
//   query Countries {
//     countries {
//       code
//       name
//       emoji
//     }
//   }
// `;

// 1850144, 1642911, 1273294, 1701668, 1796236, 1174872, 1816670, 5128581, 1699805, 524901, 2988507