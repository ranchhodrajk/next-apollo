import React,{useState,useEffect} from 'react'
import { useRouter } from 'next/router'
import s from '../styles/State.module.scss'
import { Row, Col, Button,Divider } from "antd";
import LiveClock from '../Components/LiveClock/LiveClock';
import { useQuery } from "@apollo/client";
import {COUNTRY} from '../Query/Country/Country'
import { kelvinToC, kelvinToF } from '../utilities/Converter'

const state = () => {

    const [ftoc, setftoc] = useState(true)
    const [locationData, setlocationData] = useState()

    const router = useRouter()
    const { state } = router.query
    console.log('slug',state);

    let name=state;
  
    const { loading, error, data } = useQuery(COUNTRY, {
        variables: {name},
    });

    console.log('asdasdasdasdd',data);

    useEffect(() => {
        setlocationData(data?.getCityByName)
        
      }, [data])

    const onClickFtoC = ()=>{
        console.log('ftoc',ftoc);
        setftoc(!ftoc);
    }

    return (
        <div className={s.rowOne}>
        <div
          style={{ backgroundImage: "url(/Assets/homeCover3.jpg)" }}
          className={s.rowOneRow}
        >
          <Row>
            <Col lg={24} className={s.rowHead}>
            {locationData?.name}
            </Col>
          </Row>

          <Row className={s.desRow}>
            <Col lg={6}>
              <div className={s.degree}>
                <div className={s.degreeLabel}>Feels Like</div>
                <div className={s.degreeNum}>
                  <div className={s.num}>{ftoc ? kelvinToC(locationData?.weather?.temperature?.feelsLike) : kelvinToF(locationData?.weather?.temperature?.feelsLike)}</div>
                  <div className={s.o}>o</div>
                </div>
                <div className={s.celFer}>
                  <button className={ ftoc ? '' : 'nonactive'} onClick={onClickFtoC}>C</button>
                  <Divider type="vertical"  style={{backgroundColor:'white', margin:'0px'}}/>
                  <button className={ !ftoc ? '' : 'nonactive'} onClick={onClickFtoC}>F</button>
                </div>
              </div>
            </Col>
            <Col lg={12}>
              <div className={s.summary}>
                <Row align="middle">
                  <Col lg={8} md={8} xs={12}>
                    <div>
                        <img src={`http://openweathermap.org/img/wn/${locationData?.weather?.summary?.icon}@2x.png`} alt="iconWeather" />      
                    </div>
                  </Col>
                  <Col lg={8} md={8} xs={12}>
                    <div>
                      <div className={s.summaryHead}>Actual</div>
                      <div className={s.summaryData}>{ ftoc ? kelvinToC(locationData?.weather?.temperature?.actual) : kelvinToF(locationData?.weather?.temperature?.actual)}&deg;</div>
                    </div>
                  </Col>
                  <Col lg={8} md={8} xs={12}>
                    <div>
                      <div className={s.summaryHead}>Humadity</div>
                      <div className={s.summaryData}>{locationData?.weather?.clouds?.humidity}%</div>
                    </div>
                  </Col>
                  
                  <Col lg={8} md={8} xs={12}>
                    <div className={s.lastTow}>
                      <div className={s.summaryHead}>Summary</div>
                      <div className={s.summaryData}>{locationData?.weather?.summary?.description}</div>
                    </div>
                  </Col>
                  
                  <Col lg={8} md={8} xs={12}>
                    <div className={s.lastTow}>
                      <div className={s.summaryHead}>Wind Degree</div>
                      <div className={s.summaryData}>{locationData?.weather?.wind?.deg}&deg;</div>
                    </div>
                  </Col>
                  <Col lg={8} md={8} xs={12}>
                    <div>
                      <div className={s.summaryHead}>Wind Speed</div>
                      <div className={s.summaryData}>{locationData?.weather?.wind?.speed} m/s</div>
                    </div>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col lg={6}>
              <LiveClock/>
            </Col>
          </Row>
        </div>
      </div>
    )
}

export default state
