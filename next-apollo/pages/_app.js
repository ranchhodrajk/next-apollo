import "../styles/globals.scss";
import 'antd/dist/antd.css';
import { ApolloProvider } from "@apollo/client";
import client from "../apollo-client";
import Layout from "../Components/Layout";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

function MyApp({ Component, pageProps }) {
  return(
  <ApolloProvider client={client}>
    <Layout>
      <Component {...pageProps} />
    </Layout>
  </ApolloProvider>
  )
}

export default MyApp;
