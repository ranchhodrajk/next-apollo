import React from "react";
import s from "../styles/SignIn.module.scss";
import { Row, Col, Button, Input, Form, Checkbox } from "antd";
import { MailOutlined, LockOutlined } from "@ant-design/icons";
const signIn = () => {
  const onFinish = (values) => {
    console.log("Received values of form: ", values);
  };

  return (
    <div
      className={s.main}
      style={{ backgroundImage: "url(/Assets/signInBg4.png)" }}
    >
      <div className={s.form}>
        <Row justify="center" className={s.mainContainer}>
          <Col xs={24}>
            <Row justify="center">
              <Col lg={12} className={s.rowLeft}>
                <div className={s.leftHead}>Weather on your way</div>
                <div className={s.leftDes}>
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                  Dolorum neque ab nostrum dicta doloremque architecto, iste non
                  aliquam mollitia ipsum provident magni veniam adipisci nobis
                  facere quam, illum alias voluptatibus!
                </div>
                <div className={`${s.leftSignUp} CustomBtn`}>
                  <Button type="primary" danger ghost >
                    Sign Up
                  </Button>
                </div>
              </Col>
              <Col lg={8} className={s.rowRight}>
                <div className={s.loginForm}>
                  <div className={s.formHead}>Sign In</div>
                  <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{
                      remember: true,
                    }}
                    onFinish={onFinish}
                  >
                    <Form.Item
                      name="email"
                      rules={[
                        {
                          required: true,
                          message: "Please input your Email!",
                        },
                        
                      ]}
                      
                      className="custInput"
                    >
                      <Input
                        prefix={
                            <MailOutlined className="site-form-item-icon" />
                        }
                        
                        placeholder="Email"
                      />
                    </Form.Item>
                    <Form.Item
                      name="password"
                      rules={[
                        {
                          required: true,
                          message: "Please input your Password!",
                        },
                      ]}
                      className="custInput"
                    >
                      <Input
                        prefix={
                          <LockOutlined className="site-form-item-icon" />
                        }
                        type="password"
                        placeholder="Password"
                      />
                    </Form.Item>
                    <Form.Item>
                      <Form.Item
                        name="remember"
                        valuePropName="checked"
                        noStyle
                      >
                        <Checkbox>Remember me</Checkbox>
                      </Form.Item>

                      <a className="login-form-forgot" href="">
                        Forgot password
                      </a>
                    </Form.Item>

                    <Form.Item className='CustomBtn'>
                      <Button
                        block
                        danger
                        ghost
                        type="primary"
                        htmlType="submit"
                        className="login-form-button"
                      >
                        Sign In
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default signIn;
