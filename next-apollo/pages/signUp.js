import React from "react";
import s from "../styles/SignUp.module.scss";
import { Row, Col, Button, Input, Form, Checkbox } from "antd";
import { MailOutlined, LockOutlined,EditOutlined } from "@ant-design/icons";

const signUp = () => {
  const onFinish = (values) => {
    console.log("Received values of form: ", values);
  };

  return (
    <div
      className={s.main}
      style={{ backgroundImage: "url(/Assets/signInBg4.png)" }}
    >
      <div className={s.form}>
        <Row justify="center" className={s.mainContainer}>
          <Col xs={24}>
            <Row justify="center">
              
              <Col lg={12} className={s.rowLeft}>
                <div className={s.leftHead}>Already have an account ?</div>
                <div className={s.leftDes}>
                  Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                  Dolorum neque ab nostrum dicta doloremque architecto, iste non
                  aliquam mollitia ipsum provident magni veniam adipisci nobis
                  facere quam, illum alias voluptatibus!
                </div>
                <div className={`${s.leftSignUp} CustomBtn`}>
                  <Button type="primary" danger ghost>
                    Sign In
                  </Button>
                </div>
              </Col>
              <Col lg={8} className={s.rowRight}>
                <div className={s.loginForm}>
                  <div className={s.formHead}>Sign Up</div>
                  <Form
                    name="normal_login"
                    className="login-form"
                    initialValues={{
                      remember: true,
                    }}
                    onFinish={onFinish}
                  >
                    <Form.Item
                      name="name"
                      rules={[
                        {
                          required: true,
                          message: "Please input your Name!",
                        },
                      ]}
                      className="custInput"
                    >
                      <Input
                        prefix={
                            <EditOutlined className="site-form-item-icon"/>
                        }
                        placeholder="Name"
                      />
                    </Form.Item>
                    <Form.Item
                      name="email"
                      rules={[
                        {
                          required: true,
                          message: "Please input your Email!",
                        },
                      ]}
                      className="custInput"
                    >
                      <Input
                        prefix={
                          <MailOutlined className="site-form-item-icon" />
                        }
                        placeholder="Email"
                      />
                    </Form.Item>
                    
                    <Form.Item
                      name="password"
                      rules={[
                        {
                          required: true,
                          message: "Please input your Password!",
                        },
                      ]}
                      className="custInput"
                    >
                      <Input
                        prefix={
                          <LockOutlined className="site-form-item-icon" />
                        }
                        type="password"
                        placeholder="Password"
                      />
                    </Form.Item>
                    <Form.Item
                      name="cpassword"
                      rules={[
                        {
                          required: true,
                          message: "Please input your confirm Password!",
                        },
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                              if (!value || getFieldValue('password') === value) {
                                return Promise.resolve();
                              }
                              return Promise.reject(new Error('The two passwords that you entered do not match!'));
                            },
                          }),
                      ]}
                      className="custInput"
                    >
                      <Input
                        prefix={
                          <LockOutlined className="site-form-item-icon" />
                        }
                        type="password"
                        placeholder="Confirm Password"
                      />
                    </Form.Item>
                    <Form.Item>
                      <Form.Item
                        name="remember"
                        noStyle
                      >
                        <Checkbox>I agree turms & condition</Checkbox>
                      </Form.Item>

                      
                    </Form.Item>

                    <Form.Item className="CustomBtn">
                      <Button
                        block
                        danger
                        ghost
                        type="primary"
                        htmlType="submit"
                        className="login-form-button"
                      >
                        Sign Up
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default signUp;
