export const kelvinToF = (F)=>{
    let x = Math.ceil((F * 9/5) - 459.67)
    return Number(x);
}

export const kelvinToC = (C)=>{
    let x= Math.ceil(C - 273.15);
    return Number(x);
}